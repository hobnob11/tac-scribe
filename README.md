# TacScribe

Writes object state from a tacview server to a PostGIS extended Postgresql
database for further processing by other systems.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tac_scribe'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install tac_scribe

## Usage

You can run the tool using the `tac_scribe` command. Use the `--help`
option for information on required arguments

### Whitelist

If you want to restrict what units get written to the database (e.g. for
performance reasons) then create a whitelist file and pass that in with
the whitelist option. For example if you only care about air units
and landing areas create a file like the following:

`whitelist.txt`

With the following content

```
Sea+Watercraft+AircraftCarrier
Air+Rotorcraft
Air+FixedWing
Ground+Static+Aerodrome
Navaid+Static+Bullseye
```

and add `-w whitelist.txt` to the command-line.

The unit type is the type provided by Tacview and it must be an exact match

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then,
run `rake test` to run the tests. You can also run `bin/console` for an
interactive prompt that will allow you to experiment.

### Postgresql and PostGIS

This gem requires postgresql and PostGIS available and listening on an
accessible network port.

See the `db` folder for information on running the database migrations.

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/overlord-bot/tac-scribe.

### Adding Data

There are incomplete data-files for this project whose completion would
be very helpful. See the `data` folder. Current tasks needed are:

* Complete the list of all airfields in DCS with their position information

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
