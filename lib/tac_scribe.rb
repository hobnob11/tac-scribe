# frozen_string_literal: true

require_relative 'tac_scribe/version'
require_relative 'tac_scribe/daemon'

# Top-level name space for all TacScribe clases
module TacScribe
end
